﻿namespace Iiko.Payments.Shared
{
    /// <summary>
    /// Запрос на закрытие счёта на номер
    /// </summary>
    public class PostRequest
    {
        /// <summary>
        /// Номер счёта
        /// </summary>
        public string FolioNo { get; set; }

        /// <summary>
        /// Детализация заказа
        /// </summary>
        public Order Order { get; set; }
    }
}