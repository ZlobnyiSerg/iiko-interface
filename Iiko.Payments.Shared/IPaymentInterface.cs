﻿namespace Iiko.Payments.Shared
{
    public interface IPaymentInterface
    {
        /// <summary>
        /// Осуществляет поиск счетов во внешней системе
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SearchFoliosResponse SearchFolio(SearchFoliosRequest request);

        /// <summary>
        ///     Закрытие заказа на номер/профиль или оплата баллами лояльности
        /// </summary>
        /// <param name="folioNo">Номер счёта, на который закрывается заказ</param>
        /// <param name="order">Состав заказа</param>
        void Post(string folioNo, Order order);

        /// <summary>
        ///     Отправка информации о совершённых оплатах во внешнюю систему. Используется в случаях оплаты наличными/кредиткой.
        /// </summary>
        /// <param name="orders">Заказ(ы) по которым совершена оплата</param>
        void AddPayment(Order[] orders);
    }
}
