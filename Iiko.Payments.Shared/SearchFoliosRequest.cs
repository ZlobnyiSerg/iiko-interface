﻿using System.ComponentModel.DataAnnotations;

namespace Iiko.Payments.Shared
{
    /// <summary>
    ///     Запрос на список счетов для закрытия заказа
    /// </summary>
    public class SearchFoliosRequest
    {
        /// <summary>
        ///     Идентификатор для поиска среди доступных для закрытия счетов
        /// </summary>
        [Required]
        public string Query { get; set; }

        /// <summary>
        ///     Область поиска
        /// </summary>
        public SearchArea Area { get; set; }       

        /// <summary>
        ///     Детализация заказа (и оплат)
        /// </summary>
        public Order Order { get; set; }
    }

    /// <summary>
    ///     Ответ на поиск счетов
    /// </summary>
    public class SearchFoliosResponse
    {
        /// <summary>
        ///     Найденные согласно критерию поиска счета (гости/комнаты/групповые брони и тп)
        /// </summary>
        public FolioInfo[] Folios { get; set; }
    }

    /// <summary>
    /// Области поиска
    /// </summary>
    public class SearchArea
    {
        /// <summary>
        /// По номеру счёта
        /// </summary>
        public bool FolioNo { get; set; }
        
        /// <summary>
        /// По номеру комнаты
        /// </summary>
        public bool RoomNo { get; set; }
        
        /// <summary>
        /// По карте-ключу
        /// </summary>
        public bool KeyCard { get; set; }
        
        /// <summary>
        /// По карте лояльности
        /// </summary>
        public bool LoyaltyCard { get; set; }
    }

    /// <summary>
    ///     Описание счёта
    /// </summary>
    public class FolioInfo
    {
        /// <summary>
        ///     Уникальный идентификатор
        /// </summary>
        public string FolioNo { get; set; }

        /// <summary>
        ///     Название (для гостя - ФИО, для группы или компании - название группы или компании)
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        ///     Баланс счёта
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        ///     Признак, что заказ может быть закрыт на этот счёт
        /// </summary>
        public bool CanAccept { get; set; }
        
        /// <summary>
        ///     Максимальная сумма, которая может быть оплачена баллами лояльности
        /// </summary>
        public decimal BonusPaymentLimit { get; set; }
    }
}
