﻿using System.ComponentModel.DataAnnotations;

namespace Iiko.Payments.Shared
{
    /// <summary>
    ///     Заказ (перечень позиций)
    /// </summary>
    public class Order
    {
        /// <summary>
        ///     Уникальный номер заказа
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Официант
        /// </summary>
        public string Waiter { get; set; }

        /// <summary>
        ///     Точка продаж
        /// </summary>
        public string PointOfSale { get; set; }

        /// <summary>
        ///     Идентификатор карты лояльности. Может использоваться для начисления баллов за заказ
        /// </summary>
        public string LoyaltyCardNo { get; set; }

        /// <summary>
        ///     Детализация заказа
        /// </summary>
        [Required]
        public OrderItem[] Items { get; set; }

        /// <summary>
        ///     Оплаты
        /// </summary>        
        public Payment[] Payments { get; set; }
    }

    /// <summary>
    ///     Товарная позиция заказа
    /// </summary>
    public class OrderItem
    {
        /// <summary>
        ///     Артикул товара
        /// </summary>
        [Required]
        public string ArticleNo { get; set; }

        /// <summary>
        ///     Артикул товарной группы
        /// </summary>
        public string GroupArticleNo { get; set; }

        /// <summary>
        ///     Наименование товара/блюда
        /// </summary>
        [Required] 
        public string Name { get; set; }

        /// <summary>
        ///     Количество
        /// </summary>
        [Required]
        public decimal Quantity { get; set; }

        /// <summary>
        ///     Цена
        /// </summary>
        [Required]
        public decimal Amount { get; set; }

        /// <summary>
        ///     Цена до применения скидки
        /// </summary>
        public decimal AmountBeforeDiscount { get; set; }

        /// <summary>
        ///     Сумма включённого налога
        /// </summary>
        public decimal IncludedTaxAmount { get; set; }
    }

    /// <summary>
    ///     Оплата заказа
    /// </summary>
    public class Payment
    {
        /// <summary>
        ///     Код типа оплаты
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        ///     Сумма оплаты
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        ///     Номер чека
        /// </summary>
        public string ChequeNo { get; set; }
    }

    /// <summary>
    ///     Способ оплаты
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        ///     Наличные
        /// </summary>
        Cash = 1,

        /// <summary>
        ///     Банковская карта
        /// </summary>
        CreditCard = 2,

        /// <summary>
        ///     Закрытие на счёт
        /// </summary>
        Folio = 3,

        /// <summary>
        ///     Оплата баллами системы лояльности
        /// </summary>
        Loyalty = 4,

        /// <summary>
        ///     Другой способ оплаты
        /// </summary>
        Other = 5
    }
}
